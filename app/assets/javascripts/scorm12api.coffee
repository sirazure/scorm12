ScormError =
  noError:            {code:"0",message:"No error"},
  generalError:       {code:"101",message:"General Exception"},
  wrongArguments:     {code:"201",message:"Invalid argument error"},
  noChildrenPossible: {code:"202",message:"Element cannot have children"},
  notCountable:       {code:"203",message:"Element not an array. Cannot have count"},
  notInitialized:     {code:"301",message:"Not initialized"},
  notImplemented:     {code:"401",message:"Not implemented error"},
  restrictedKeyword:  {code:"402",message:"Invalid set value, element is a keyword"},
  readOnly:           {code:"403",message:"Element is read only"},
  writeOnly:          {code:"404",message:"Element is write only"},
  incorrectDataType:  {code:"405",message:"Incorrect Data Type"}

getErrorMessage = (code) ->
  for error in ScormError
    if error.code == code
      return error.message
  return "Unknown error code"



ReadOnlyFields = ["cmi.core.student_id","cmi.core.student_name","cmi.core.credit",
                  "cmi.core.entry","cmi.core.total_time","cmi.core.lesson_mode",
                  "cmi.launch_data","cmi.comments_from_lms","cmi.student_data.mastery_score",
                  "cmi.student_data.max_time_allowed","cmi.student_data.time_limit_action"]

WriteOnlyFields = ["cmi.core.exit","cmi.core.session_time","cmi.interactions.n.id",
                   "cmi.interactions.n.objectives.n.id","cmi.interactions.n.time",
                   "cmi.interactions.n.type","cmi.interactions.n.correct_responses.n.pattern",
                   "cmi.interactions.n.weighting","cmi.interactions.n.student_response",
                   "cmi.interactions.n.result","cmi.interactions.n.latency"]

LessonStatusVocabulary = ["passed","completed","failed","incomplete","browsed","not attempted"]

WithChildren = ["cmi.core._children","cmi.core.score._children","cmi.objectives._children",
                "cmi.objectives.n.score._children","cmi.student_data._children",
                "cmi.student_preference._children","cmi.interactions._children"]

WithCount = ["cmi.objectives._count","cmi.interactions_count","cmi.interactions.n.objectives_count",
             "cmi.interactions.n.correct_responses._count"]

Scorm12Validators = [
  {fieldName:"cmi.core.lesson_location", defaultValue: "", validate: (value)-> return true},
  {fieldName:"cmi.core.lesson_status",defaultValue:"not attempted", validate: (value) -> isInDictionary(LessonStatusVocabulary,value)}
  ]

isInDictionary = (dictionary,value) ->
  if /\.[0-9]+\./i.test(value) #look for array reference
    value = value.replace(/\.[0-9]+\./ig,".n.") #replace array reference to match dict pattern

  for entry in dictionary
    if value==entry
      return true
  return false



class Scorm12API

  constructor: () ->
    @lastError = ScormError.noError
    @cmiData = {}
    @started = false
    @debug=true

  log: (message)->
    if @debug
      console.log(message)

  logError: (error)->
    @log error.code + " : " + error.message

  logLastError: ->
    @log @lastError.code + " : " + @lastError.message



  LMSInitialize: (init) ->
    @log "initialize SCORM 1.2 API"

    initResult = true
    @lastError=ScormError.noError

    if arguments.length != 1
      @lastError = ScormError.wrongArguments
      initResult = false

    else if init!=""
      @lastError = ScormError.wrongArguments
      initResult = false

    else if @started
      @lastError = ScormError.generalError
      initResult = false

    else
      jQuery.ajax(
        url:"/scorm/init",
        dataType: "json",
        async : false,
        success: (data)=>
          @cmiData = data
          @cmiData.cmi._version="3.4"
      )

      @log "everything is ok"
      @started = true;

      @log "exit with result = " +initResult+" and " + @lastError.message

    if (@lastError != ScormError.noError)
      @logLastError()
    else
      @log @cmiData

    return initResult

  LMSFinish:(emptyParam) ->
    @log "finish sco "+emptyParam
    @started=false

  LMSGetValue: (valueName) ->

    @log "getting value: "+ valueName
    @lastError=ScormError.noError
    value = {}
    if(valueName.indexOf("._children") != -1)
      x =  @getChildren(valueName)
      return x
    else if(valueName.indexOf("._count") != -1)
      return @getChildren(valueName)
    else if(isInDictionary(WriteOnlyFields, valueName))
      @lastError=ScormError.writeOnly
      return ""
    else
      if /\.[0-9]+\./i.test(valueName) #look for array reference
        valueName = valueName.replace(/\.([0-9])+\./gi,"[$1].") #change array reference to match json
      value = jsonPath(@cmiData,"$."+valueName)
      if(value==false)
        @lastError = ScormError.wrongArguments
        return ""
      else
        return value

  getChildren: (valueName) ->
    parent = valueName.replace("._children","")
    if(!isInDictionary(WithChildren,valueName))
      if(!jsonPath(@cmiData,"$."+parent))
        @lastError = ScormError.noChildrenPossible
      else
        @lastError = ScormError.wrongArguments
      return ""
    else
      return @children(jsonPath(@cmiData,"$."+parent))

  children: (cmiEntry) ->
    attributes = []
    jQuery.each(cmiEntry[0], (key, value) =>
      attributes.push(key)
    )
    return attributes

  getCount: (valueName) ->
    parent = valueName.replace("._count","")
    if(!isInDictionary(WithChildren,valueName))
      if(!jsonPath(@cmiData,"$."+parent))
        @lastError = ScormError.notCountable
      else
        @lastError = ScormError.wrongArguments
      return ""
    return @count(jsonPath(@cmiData,"$."+parent))

  count: (cmiEntry) ->
    length = 0
    jQuery.each(cmiEntry[0], (key, value) =>
      length++
    )
    return length


  LMSSetValue: (valueName, newValue) ->

    @log "setting new value: \""+newValue+"\" for field "+valueName
    @lastError=ScormError.noError

    if isInDictionary(ReadOnlyFields, valueName)
      @lastError=ScormError.readOnly
      return false

    if isInDictionary(WithCount,valueName) or isInDictionary(WithChildren,valueName)
      @lastError=ScormError.restrictedKeyword
      return false

    if /\.[0-9]+\./i.test(valueName) #look for array reference
      valueName = valueName.replace(/\.([0-9])+\./gi,"[$1].") #change array reference to match json
    object = jsonPath(@cmiData,"$."+valueName)

    if object==false
      @lastError=ScormError.wrongArguments
      return false

    validationPassed=true
    for validator in Scorm12Validators
      if validator.fieldName==valueName
         validationPassed = validator.validate(valueName)

    if !validationPassed
      @lastError=ScormError.incorrectDataType
      return false

    objectnewValue
    return true

  LMSCommit: (emptyParam) ->
    @log "commiting "+emptyParam
    @lastError=ScormError.noError

  LMSGetLastError: ->
    return @lastError

  LMSGetErrorString: (checkedCode) ->
    this.getErrorMessage(checkedCode)

  #TODO should handle additional info
  LMSGetDiagnostic : ->
    @log "get diagnostic about "+@lastError.code

    if !@started
      @lastError=ScormError.notInitialized
    return this.getErrorMessage(@lastError.code)



window.API = new Scorm12API